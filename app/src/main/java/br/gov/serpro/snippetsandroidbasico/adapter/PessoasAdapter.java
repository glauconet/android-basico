package br.gov.serpro.snippetsandroidbasico.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.model.Pessoa;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class PessoasAdapter extends ArrayAdapter<Pessoa> {
    public PessoasAdapter(Context context, ArrayList<Pessoa> Pessoas) {
        super(context, 0, Pessoas);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Pessoa Pessoa = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.pessoa_item, parent, false);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.nome_textview);
        TextView tvHome = (TextView) convertView.findViewById(R.id.profissao_textview);

        tvName.setText(Pessoa.nome);
        tvHome.setText(Pessoa.profissao);

        return convertView;
    }
}
