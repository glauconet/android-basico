package br.gov.serpro.snippetsandroidbasico.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 19/05/16.
 */
public class ComponentesUIActivity extends AppCompatActivity {

    private static final String TAG = "ComponentesUIActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.componentes_ui);

        setTitle("Componentes");

        // ************ Exercício 1 e 2: View + Button ************
        final View viewMudarDeCor = findViewById(R.id.view_mudar_de_cor);
        Button buttonMudarDeCor = (Button) findViewById(R.id.button_mudar_de_cor);
        buttonMudarDeCor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rnd = new Random();
                int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                viewMudarDeCor.setBackgroundColor(color);
                Log.d(TAG, "Cor da view alterada para: " + color);
            }
        });

        // ************ Exercício 3 e 4: TextView + EditText ************
        final EditText editTextMudarTexto = (EditText) findViewById(R.id.edittext_mudar_texto);
        final TextView textViewMudarTexto = (TextView) findViewById(R.id.textview_mudar_texto);
        Button buttonMudarTexto = (Button) findViewById(R.id.button_mudar_texto);
        buttonMudarTexto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textoBotao = editTextMudarTexto.getText().toString();
                if (!TextUtils.isEmpty(textoBotao.trim())) {
                    textViewMudarTexto.setText(
                            String.format("%s%s",
                                    getString(R.string.edittext_label),
                                    textoBotao.trim())
                    );
                }
            }
        });

        // ************ Exercício 5: ImageView ************
        final ImageView imageViewQueSeraEscondida = (ImageView) findViewById(R.id.imageview_que_sera_escondida);
        Button buttonEsconderImagem = (Button) findViewById(R.id.button_esconder_imagem);
        buttonEsconderImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageViewQueSeraEscondida.getVisibility() == View.VISIBLE) {
                    imageViewQueSeraEscondida.setVisibility(View.GONE);
                    ((Button) v).setText(R.string.button_exibir_imagem);
                } else {
                    imageViewQueSeraEscondida.setVisibility(View.VISIBLE);
                    ((Button) v).setText(R.string.button_esconder_imagem);
                }
            }
        });

        // ************ Exercício 6: CheckBox ************
        CheckBox checkBoxAlterarTexto = (CheckBox) findViewById(R.id.checkbox_alterar_texto);
        checkBoxAlterarTexto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setText(isChecked ?
                        getString(R.string.checkbox_marcado) :
                        getString(R.string.checkbox_desmarcado));
            }
        });

        // ************ Exercício 7: Switch ************
        Switch switchAlterarTexto = (Switch) findViewById(R.id.switch_alterar_texto);
        switchAlterarTexto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonView.setText(isChecked ?
                        getString(R.string.switch_ligado) :
                        getString(R.string.switch_desligado));
            }
        });

        // ************ Exercício 8: Spinner ************
        final TextView textViewProfissaoEscolhida = (TextView) findViewById(R.id.textview_profissao_escolhida);
        final Spinner spinnerProfissaoEscolhida = (Spinner) findViewById(R.id.spinner_profissao_escolhida);
        spinnerProfissaoEscolhida.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Forma não elegante
                // textViewProfissaoEscolhida.setText("Profissão escolhida: " + spinnerProfissaoEscolhida.getSelectedItem().toString());

                // Forma mais elegante
                if (view != null) {
                    textViewProfissaoEscolhida.setText(
                            String.format("%s%s",
                                    getString(R.string.profissao_escolhida),
                                    ((AppCompatTextView) view).getText().toString())
                    );
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // ************ Exercício 9: SeekBar ************
        SeekBar seekBarQuantidade = (SeekBar) findViewById(R.id.seekbar_quantidade);
        final TextView textViewQuantidade = (TextView) findViewById(R.id.textview_quantidade);
        seekBarQuantidade.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textViewQuantidade.setText(progress + " itens selecionados");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // ************ Exercício 10: ProgressBar ************
        final ProgressBar progressBarMudarProgresso = (ProgressBar) findViewById(R.id.progressbar_mudar_progresso);
        Button buttonMudarProgresso = (Button) findViewById(R.id.button_mudar_progresso);
        buttonMudarProgresso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int progress = progressBarMudarProgresso.getProgress();
                int max = progressBarMudarProgresso.getMax();

                if (progress < max) {
                    progress += 10;
                    progressBarMudarProgresso.setProgress(progress);
                } else {
                    progress = 0;
                    progressBarMudarProgresso.setProgress(progress);
                }
            }
        });

        // ************ Exercício 11: RadioButton ************
        final TextView textViewFeedback = (TextView) findViewById(R.id.textview_feedback);
        RadioGroup radioGroupFeedback = (RadioGroup) findViewById(R.id.radiogroup_feedback);
        radioGroupFeedback.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (R.id.radiobutton_sim_feedback == checkedId) {
                    textViewFeedback.setText(R.string.radiobutton_sim_feedback);
                } else if (R.id.radiobutton_nao_feedback == checkedId) {
                    textViewFeedback.setText(R.string.radiobutton_nao_feedback);
                }
            }
        });

        // ************ Exercício 12: RatingBar ************
        RatingBar ratingBarNotaCurso = (RatingBar) findViewById(R.id.ratingbar_nota_curso);
        final TextView textView = (TextView) findViewById(R.id.textview_nota_curso);
        ratingBarNotaCurso.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                String texto = getString(R.string.ratingbar_texto_1) +
                        rating +
                        getString(R.string.ratingbar_texto_2);

                switch ((int) rating) {
                    case 0:
                        texto += getString(R.string.rating_0);
                        break;
                    case 1:
                        texto += getString(R.string.rating_1);
                        break;
                    case 2:
                        texto += getString(R.string.rating_2);
                        break;
                    case 3:
                        texto += getString(R.string.rating_3);
                        break;
                    case 4:
                        texto += getString(R.string.rating_4);
                        break;
                    case 5:
                        texto += getString(R.string.rating_5);
                        break;
                }

                textView.setText(texto);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}
