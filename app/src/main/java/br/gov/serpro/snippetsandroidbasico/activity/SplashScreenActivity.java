package br.gov.serpro.snippetsandroidbasico.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.gov.serpro.snippetsandroidbasico.*;

/**
 * Criado por Marcelo Baccelli em 05/09/16.
 */
public class SplashScreenActivity extends AppCompatActivity {

    /* ---------------------------------------------------------------------------------------------
     * O EXEMPLO FOI RETIRADO DE https://www.bignerdranch.com/blog/splash-screens-the-right-way/
     * ou seja:
     * - Crie um drawable (no caso o splashscreen.xml)
     * - Crie um estilo que tenha o parent Theme.AppCompat.NoActionBar e utilize como
     *   windowBackground o drawable que criou
     * - Crie uma Activity (que será a launcher) e que tenha como atributo android:theme o estilo
     *   criado. Faça como o método onCreate abaixo
     -------------------------------------------------------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(this, br.gov.serpro.snippetsandroidbasico.MainActivity.class));
        finish();
    }
}