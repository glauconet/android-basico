package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 06/06/16.
 */
public class ScrollViewComTextViewScrollActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrollview_com_textview_com_scroll_activity);

        final TextView textViewTextoGrande = (TextView) findViewById(R.id.textview_texto_grande);
        textViewTextoGrande.setMovementMethod(new ScrollingMovementMethod());

        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textViewTextoGrande.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });

        textViewTextoGrande.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textViewTextoGrande.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }
}
