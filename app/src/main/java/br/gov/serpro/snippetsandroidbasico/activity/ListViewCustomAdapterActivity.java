package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.ListView;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.adapter.PessoasAdapter;
import br.gov.serpro.snippetsandroidbasico.model.Pessoa;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class ListViewCustomAdapterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);

        setTitle("ListView");

        ArrayList<Pessoa> pessoasArray = new ArrayList<>();
        pessoasArray.add(new Pessoa("Alberto", "Analista de Sistemas"));
        pessoasArray.add(new Pessoa("Juliana", "Dentista"));
        pessoasArray.add(new Pessoa("Marcelo", "Pintor"));
        pessoasArray.add(new Pessoa("Marcia", "Testadora"));

        ListView listView = (ListView) findViewById(R.id.listview);

        PessoasAdapter adapter = new PessoasAdapter(this, pessoasArray);

        listView.setAdapter(adapter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
