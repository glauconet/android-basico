package br.gov.serpro.snippetsandroidbasico.activity;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;

import br.gov.serpro.snippetsandroidbasico.receiver.AlarmReceiver;


public class NotificacoesActivity extends AppCompatActivity {
//    Intent intent;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        intent = getIntent();
//
//        if (AndroidBasicoApplication.isFirstTime(this, AndroidBasicoApplication.SHARED_PREFERENCES_NOTIFICACAO_EXIBIDA)) {
//            Calendar cal = new GregorianCalendar();
//            cal.setTimeInMillis(System.currentTimeMillis());
//            cal.set(Calendar.DAY_OF_MONTH, 4);
//            cal.set(Calendar.HOUR_OF_DAY, 21);
//            cal.set(Calendar.MINUTE, 00);
//            cal.set(Calendar.SECOND, 0);
//            cal.set(Calendar.MILLISECOND, 0);
//            Log.d("HORA AGENDADA", cal.getTime().toString());
//            agendarNotificacao(cal);
//            Answers.getInstance().logContentView(new ContentViewEvent()
//                    .putContentName("Notificação Agendada na Primeira Execução")
//                    .putContentType("Log de Evento")
//                    .putContentId("notificacao-01"));
//        }
//
//
//        TextView textViewCriarAlarme = (TextView) findViewById(R.id.criar_alarme);
//        textViewCriarAlarme.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Obtém o horário para daqui 30 segundos
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTimeInMillis(System.currentTimeMillis() + 30000);
//
//                agendarNotificacao(calendar);
//                Toast.makeText(getApplicationContext(), "Agendou para daqui 30s", Toast.LENGTH_LONG).show();
//                Answers.getInstance().logContentView(new ContentViewEvent()
//                        .putContentName("Notificação Agendada para 30 Segundos")
//                        .putContentType("Log de Evento")
//                        .putContentId("notificacao-02"));
//            }
//        });
//
//        TextView textViewCriarNotificacao = (TextView) findViewById(R.id.criar_notificao);
//        textViewCriarNotificacao.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                criarNotificao();
//            }
//        });
//
//    }
//
//    private void criarNotificao() {
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setTicker("Texto da notificação")
//                .setColor(ContextCompat.getColor(this, R.color.colorNotification))
//                .setSmallIcon(R.drawable.ic_stat_cake)
//                .setAutoCancel(true)
//                .setContentTitle("Título da notificação")
//                .setContentText("Descrição");
//
//        // Cria um intent explícito
//        Intent resultIntent = new Intent(this, TratamentoNotificaoActivity.class);
//
//        //Cria um objeto de pilha de tarefas artificial (back stack) para a Activity iniciada
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addParentStack(TratamentoNotificaoActivity.class);
//        stackBuilder.addNextIntent(resultIntent);
//
//        PendingIntent resultPendingIntent =
//                stackBuilder.getPendingIntent(
//                        0,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                );
//        notificationBuilder.setContentIntent(resultPendingIntent);
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(12345, notificationBuilder.build());
//    }
//
//
//    private void agendarNotificacao(Calendar calendar) {
//        //Informa o receiver
//        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
//        PendingIntent pendingIntentReceiver = PendingIntent.getBroadcast(
//                this, //Contexto
//                0, //Request code
//                alarmIntent, //Intent que tratará o alarme na hora correta
//                PendingIntent.FLAG_UPDATE_CURRENT //Flags
//        );
//
//        //Agenda o alarme
//        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        long milisegundosEvento = calendar.getTimeInMillis();
//
//        final int SDK_INT = Build.VERSION.SDK_INT;
//
//        if (SDK_INT < Build.VERSION_CODES.KITKAT) {
//            am.set(AlarmManager.RTC_WAKEUP, milisegundosEvento, pendingIntentReceiver);
//        } else if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M) {
//            am.setExact(AlarmManager.RTC_WAKEUP, milisegundosEvento, pendingIntentReceiver);
//        } else if (SDK_INT >= Build.VERSION_CODES.M) {
//            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, milisegundosEvento, pendingIntentReceiver);
//        }
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (intent != null && intent.getData() != null) {
//            onNewIntent(intent);
//        }
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        if (intent.getDataString() != null) {
//            Uri uri = Uri.parse(intent.getDataString());
//            int id = Integer.parseInt(uri.getHost());
//            abrirExemplo(id);
//        }
//
//    }
}
