package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 07/06/16.
 */
public class TratamentoNotificaoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tratamento_notificacao_activity);
    }
}
