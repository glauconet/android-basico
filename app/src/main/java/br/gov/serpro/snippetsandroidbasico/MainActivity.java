package br.gov.serpro.snippetsandroidbasico;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import br.gov.serpro.snippetsandroidbasico.activity.AnimationDrawableActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ComponentesUIActivity;
import br.gov.serpro.snippetsandroidbasico.activity.DatePickerDialogActivity;
import br.gov.serpro.snippetsandroidbasico.activity.GridViewActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LayoutFrameLayoutActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LayoutFrameLayoutSlidesActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LayoutGridLayoutActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LayoutLinearLayoutActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LayoutRelativeLayoutActivity;
import br.gov.serpro.snippetsandroidbasico.activity.LerJSONActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ListViewActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ListViewCustomAdapterActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ListViewCustomAdapterSelectActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ListViewCustomAdapterSelectRemoveActivity;
import br.gov.serpro.snippetsandroidbasico.activity.MenuOpcoesActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ScrollViewComTextViewScrollActivity;
import br.gov.serpro.snippetsandroidbasico.activity.ShapeDrawableActivity;
import br.gov.serpro.snippetsandroidbasico.activity.StateDrawableActivity;
import br.gov.serpro.snippetsandroidbasico.activity.TextViewScrollActivity;

/**
 * Criado por Marcelo Baccelli em 05/09/16.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Trata os cliques no drawer (menu lateral)
        int id = item.getItemId();

        if (id == R.id.nav_datepickerdialog) {
            startActivity(new Intent(MainActivity.this, DatePickerDialogActivity.class));
        } else if (id == R.id.nav_animation_drawable) {
            startActivity(new Intent(MainActivity.this, AnimationDrawableActivity.class));
        } else if (id == R.id.nav_shape_drawable) {
            startActivity(new Intent(MainActivity.this, ShapeDrawableActivity.class));
        } else if (id == R.id.nav_state_drawable) {
            startActivity(new Intent(MainActivity.this, StateDrawableActivity.class));
        } else if (id == R.id.nav_frame_layout_slides) {
            startActivity(new Intent(MainActivity.this, LayoutFrameLayoutSlidesActivity.class));
        } else if (id == R.id.nav_frame_layout) {
            startActivity(new Intent(MainActivity.this, LayoutFrameLayoutActivity.class));
        } else if (id == R.id.nav_grid_layout) {
            startActivity(new Intent(MainActivity.this, LayoutGridLayoutActivity.class));
        } else if (id == R.id.nav_linear_layout) {
            startActivity(new Intent(MainActivity.this, LayoutLinearLayoutActivity.class));
        } else if (id == R.id.nav_relative_layout) {
            startActivity(new Intent(MainActivity.this, LayoutRelativeLayoutActivity.class));
        } else if (id == R.id.nav_componentes_basicos) {
            startActivity(new Intent(MainActivity.this, ComponentesUIActivity.class));
        } else if (id == R.id.nav_gridview) {
            startActivity(new Intent(MainActivity.this, GridViewActivity.class));
        } else if (id == R.id.nav_listview) {
            startActivity(new Intent(MainActivity.this, ListViewActivity.class));
        } else if (id == R.id.nav_listview_custom) {
            startActivity(new Intent(MainActivity.this, ListViewCustomAdapterActivity.class));
        } else if (id == R.id.nav_listview_select) {
            startActivity(new Intent(MainActivity.this, ListViewCustomAdapterSelectActivity.class));
        } else if (id == R.id.nav_listview_select_remove) {
            startActivity(new Intent(MainActivity.this, ListViewCustomAdapterSelectRemoveActivity.class));
        } else if (id == R.id.nav_scrollview_com_textview) {
            startActivity(new Intent(MainActivity.this, ScrollViewComTextViewScrollActivity.class));
        } else if (id == R.id.nav_textview_scroll) {
            startActivity(new Intent(MainActivity.this, TextViewScrollActivity.class));
        } else if (id == R.id.nav_toast_curta_duracao) {
            Toast.makeText(MainActivity.this, "Toast curta duração", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_toast_longa_duracao) {
            Toast.makeText(MainActivity.this, "Toast longa duração", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_alert_dialog) {
            exibirAlertDialog();
        } else if (id == R.id.nav_custom_dialog) {
            exibirCustomDialog();
        } else if (id == R.id.nav_menu_opcoes) {
            startActivity(new Intent(MainActivity.this, MenuOpcoesActivity.class));
        } else if (id == R.id.nav_ler_json) {
            startActivity(new Intent(MainActivity.this, LerJSONActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void exibirCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // remove o título. ANTES do setContentView
        dialog.setCanceledOnTouchOutside(false); // não fecha o dialog se clicar fora dele
        dialog.setContentView(R.layout.custom_dialog);

        Button dialogButton = (Button) dialog.findViewById(R.id.button_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "//TODO implementar destruição", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void exibirAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("DESTRUIÇÃO DO MUNDO");

        alertDialogBuilder
                .setMessage("Deseja prosseguir?")
                .setCancelable(false)
                .setPositiveButton("Com certeza", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(MainActivity.this, "//TODO implementar destruição", Toast.LENGTH_LONG).show();
                        dialog.cancel(); // fecha o dialog
                    }
                })
                .setNegativeButton("Talvez amanhã", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel(); // fecha o dialog
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show(); // exibe o dialog
    }
}
