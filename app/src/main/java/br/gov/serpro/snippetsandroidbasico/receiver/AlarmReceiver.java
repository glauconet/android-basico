package br.gov.serpro.snippetsandroidbasico.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.activity.ItsComingActivity;

/**
 * Criado por Marcelo Baccelli em 03/06/16.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static int NOTIFICATION_ID = 123456789;
    private long[] mVibratePattern = {0, 200, 200, 300, 300, 500, 500, 500, 500};

    @Override
    public void onReceive(Context context, Intent intent) {
        //Criando intent da activity
        Intent notificationIntentActivity = new Intent(context, ItsComingActivity.class);
        notificationIntentActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        //Criando back stack
        PendingIntent pendingIntent = TaskStackBuilder.create(context)
                .addNextIntentWithParentStack(notificationIntentActivity)
                .getPendingIntent(123, PendingIntent.FLAG_CANCEL_CURRENT);

        //Criando a notificação
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setTicker("1")
                .setColor(ContextCompat.getColor(context, R.color.colorNotification))
                .setSmallIcon(R.drawable.ic_stat_cake)
                .setAutoCancel(true)
                .setContentTitle("Treinamento Mobile Android")
                .setContentText("Android Cake!")
                .setContentIntent(pendingIntent);

        notificationBuilder.setVibrate(mVibratePattern);

        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(uri);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }
}
