package br.gov.serpro.snippetsandroidbasico.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 05/09/16.
 */
public class DatePickerDialogActivity extends AppCompatActivity {

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datepickerdialog_activity);

        // Cria um formatador de data (note o tipo de formatação passado)
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

        //Recupera o edittext
        final EditText editText = (EditText) findViewById(R.id.editTextDatePickerDialog);
        /**
         * ATENÇÃO: a tag android:focusable="false" faz com que não seja necessário um duplo
         * "clique" na textview para exibir o dialog
         */

        // Trata o clique simples no edittext
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });

        criarDatePickerDialog(editText);
    }

    private void criarDatePickerDialog(final EditText editText) {
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            // Método que é chamado ao definir uma data no dialog
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                // Insere a data (formatada) no campo EditText
                editText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
}
