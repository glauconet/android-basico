package br.gov.serpro.snippetsandroidbasico.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.model.Contato;

/**
 * Criado por Marcelo Baccelli em 01/06/16.
 */
public class ContatosGridAdapter extends ArrayAdapter<Contato> {
    Context context;

    public ContatosGridAdapter(Context context, ArrayList<Contato> contatosList) {
        super(context, 0, contatosList);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contato contato = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.contato_grid_item, parent, false);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.textview_contato);
        ImageView ivProfissao = (ImageView) convertView.findViewById(R.id.imageview_contato);
        tvName.setText(contato.nome);
        ivProfissao.setImageDrawable(ContextCompat.getDrawable(context, contato.imagemId));

        return convertView;
    }
}

