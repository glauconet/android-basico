package br.gov.serpro.snippetsandroidbasico.model;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class Pessoa {
    public String nome;
    public String profissao;

    public Pessoa(String nome, String profissao) {
        this.nome = nome;
        this.profissao = profissao;
    }
}

