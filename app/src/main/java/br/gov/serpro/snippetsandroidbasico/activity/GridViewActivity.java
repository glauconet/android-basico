package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.adapter.ContatosGridAdapter;
import br.gov.serpro.snippetsandroidbasico.model.Contato;

/**
 * Criado por Marcelo Baccelli em 01/06/16.
 */
public class GridViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridview_activity);

        setTitle("GridView");

        GridView listView = (GridView) findViewById(R.id.gridview);

        ArrayList<Contato> contatosList = new ArrayList<>();
        contatosList.add(new Contato("Almir Saúde", R.drawable.almir_saude));
        contatosList.add(new Contato("Andre Tetto", R.drawable.andre_tetto));
        contatosList.add(new Contato("Daniela Machado", R.drawable.daniela_machado));
        contatosList.add(new Contato("Eduardo Pelais", R.drawable.eduardo_pelais));
        contatosList.add(new Contato("Flavio Augusto Silva", R.drawable.flavio_augusto_silva));
        contatosList.add(new Contato("Gina Moraes", R.drawable.gina_moraes));
        contatosList.add(new Contato("Glauco Silva de Oliveira", R.drawable.glauco_silva_de_oliveira));
        contatosList.add(new Contato("Luis Antonio Salles", R.drawable.luis_antonio_salles));
        contatosList.add(new Contato("Manabu Gushiken", R.drawable.manabu_gushiken));
        contatosList.add(new Contato("Marcos Akira Hirota", R.drawable.marcos_akira_hirota));
        contatosList.add(new Contato("Monica Rodrigues de Souza", R.drawable.monica_rodrigues_de_souza));
        contatosList.add(new Contato("Osmar Vicente Cecilio Júnior", R.drawable.osmar_vicente_cecilio_junior));
        contatosList.add(new Contato("Roberta Fernandes Monteiro", R.drawable.roberta_fernandes_monteiro));
        contatosList.add(new Contato("Roberto Kazuo Fukuda", R.drawable.roberto_kazuo_fukuda));
        contatosList.add(new Contato("Rodolfo Bottossi Analio", R.drawable.rodolfo_bottossi_analio));
        contatosList.add(new Contato("Rodolfo Jose Vidotte", R.drawable.rodolfo_jose_vidotte));

        ContatosGridAdapter adapter = new ContatosGridAdapter(this, contatosList);

        listView.setAdapter(adapter);
    }
}
