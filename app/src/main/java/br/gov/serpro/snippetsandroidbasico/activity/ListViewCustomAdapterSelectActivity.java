package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.adapter.PessoasAdapterSelect;
import br.gov.serpro.snippetsandroidbasico.model.Pessoa;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class ListViewCustomAdapterSelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_select);

        setTitle("ListView");

        ArrayList<Pessoa> pessoasArray = new ArrayList<>();
        pessoasArray.add(new Pessoa("Alberto", "Analista de Sistemas"));
        pessoasArray.add(new Pessoa("Juliana", "Dentista"));
        pessoasArray.add(new Pessoa("Marcelo", "Pintor"));
        pessoasArray.add(new Pessoa("Marcia", "Testadora"));

        ListView listView = (ListView) findViewById(R.id.listview);

        PessoasAdapterSelect adapter = new PessoasAdapterSelect(this, pessoasArray);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView excluirImageView = (ImageView) view.findViewById(R.id.excluir_imageview);
                excluirImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ListViewCustomAdapterSelectActivity.this, "Clicou no excluir", Toast.LENGTH_LONG).show();
                    }
                });
                Toast.makeText(ListViewCustomAdapterSelectActivity.this, "Clicou no item", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
