package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.adapter.PessoasAdapterSelect;
import br.gov.serpro.snippetsandroidbasico.model.Pessoa;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class ListViewCustomAdapterSelectRemoveActivity extends AppCompatActivity {

    PessoasAdapterSelect adapter;
    private int contadorProfissao = 1;
    private int contadorNome = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_select);

        setTitle("ListView");

        final ArrayList<Pessoa> pessoasArray = new ArrayList<>();
        pessoasArray.add(new Pessoa("Alberto", "Analista de Sistemas"));
        pessoasArray.add(new Pessoa("Juliana", "Dentista"));
        pessoasArray.add(new Pessoa("Marcelo", "Pintor"));
        pessoasArray.add(new Pessoa("Marcia", "Testadora"));

        final ListView listView = (ListView) findViewById(R.id.listview);

        adapter = new PessoasAdapterSelect(this, pessoasArray);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                ImageView excluirImageView = (ImageView) view.findViewById(R.id.excluir_imageview);
                excluirImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ListViewCustomAdapterSelectRemoveActivity.this, "Clicou no excluir", Toast.LENGTH_LONG).show();
                        pessoasArray.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                Toast.makeText(ListViewCustomAdapterSelectRemoveActivity.this, "Clicou no item", Toast.LENGTH_LONG).show();
            }
        });

        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pessoasArray.add(new Pessoa("Nome " + contadorNome, "Profissão " + contadorProfissao));
                contadorNome++;
                contadorProfissao++;
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
