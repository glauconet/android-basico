package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import br.gov.serpro.snippetsandroidbasico.R;
import br.gov.serpro.snippetsandroidbasico.handler.HTTPDataHandler;
import br.gov.serpro.snippetsandroidbasico.model.Color;
import br.gov.serpro.snippetsandroidbasico.model.Colors;

/**
 * Criado por Marcelo Baccelli em 06/09/2016
 */
public class LerJSONActivity extends AppCompatActivity {

    /* *********************************************************************
     * LEIA AQUI DIABO: https://gist.github.com/wh1tney/2ad13aa5fbdd83f6a489
     * *********************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ler_json_activity);

        // Obtém o textview (para poder obter o conteúdo)
        TextView textView = (TextView) findViewById(R.id.url);

        // Obtém o conteúdo do textview (o que está escrito)
        final String url = textView.getText().toString();

        // Ao clicar no botão, chama a AsyncTask para buscar o conteúdo da URL
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ProcessJSON().execute(url);
            }
        });

    }

    private class ProcessJSON extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... strings) {
            String urlString = strings[0];

            // Obtém o conteúdo da URL
            HTTPDataHandler hh = new HTTPDataHandler();
            String stream = hh.GetHTTPData(urlString);

            return stream;
        }

        protected void onPostExecute(String stream) {
            TextView tv = (TextView) findViewById(R.id.text);

            // Se não estiver vazio o conteúdo, faz o parser com o GSON e exibe no textview
            if (stream != null) {
                Gson gson = new Gson();
                Colors colors = gson.fromJson(stream, Colors.class);

                for (Color color : colors.colors) {
                    Log.d("AAAAAH", color.colorName);
                    tv.append("\nCor: " + color.colorName + " - hexa: " + color.hexValue);
                }

            }
        }
    }
}