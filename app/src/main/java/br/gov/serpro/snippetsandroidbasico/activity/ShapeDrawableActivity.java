package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import br.gov.serpro.snippetsandroidbasico.R;

public class ShapeDrawableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shape_drawable_activity);
    }
}
