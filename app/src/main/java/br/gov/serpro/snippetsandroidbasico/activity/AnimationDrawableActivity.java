package br.gov.serpro.snippetsandroidbasico.activity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import br.gov.serpro.snippetsandroidbasico.R;

public class AnimationDrawableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_drawable_activity);

        ImageView img = (ImageView) findViewById(R.id.imageview_animacao);
        img.setBackgroundResource(R.drawable.animation_1);

        AnimationDrawable frameAnimation;

        frameAnimation = (AnimationDrawable) img.getBackground();
        frameAnimation.start();
    }
}
