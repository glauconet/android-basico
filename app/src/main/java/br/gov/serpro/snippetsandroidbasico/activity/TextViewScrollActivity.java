package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 06/06/16.
 */
public class TextViewScrollActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.textview_com_scroll_activity);

        TextView textViewTextoGrande = (TextView) findViewById(R.id.textview_texto_grande);
        textViewTextoGrande.setMovementMethod(new ScrollingMovementMethod());
    }
}
