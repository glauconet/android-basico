package br.gov.serpro.snippetsandroidbasico.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import br.gov.serpro.snippetsandroidbasico.R;

/**
 * Criado por Marcelo Baccelli em 08/06/16.
 */
public class MenuOpcoesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_opcoes_activity);

        setTitle("Menu de opções");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Infla o menu correspondente
        inflater.inflate(R.menu.menu_opcoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Trata o clique em itens do menu
        switch (item.getItemId()) {
            case R.id.compartilhar:
                Toast.makeText(this, "Clicou em COMPARTILHAR", Toast.LENGTH_LONG).show();
                return true;
            case R.id.criar_novo:
                Toast.makeText(this, "Clicou em CRIAR NOVO", Toast.LENGTH_LONG).show();
                return true;
            case R.id.abrir:
                Toast.makeText(this, "Clicou em ABRIR", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
