package br.gov.serpro.snippetsandroidbasico.model;

/**
 * Criado por Marcelo Baccelli em 30/05/16.
 */
public class Contato {
    public String nome;
    public int imagemId;

    public Contato(String nome, int imagemId) {
        this.nome = nome;
        this.imagemId = imagemId;
    }
}

