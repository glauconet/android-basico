# Este projeto faz parte da ajuda aos alunos do **Treinamento Android - Módulo Básico**.

Sempre iremos evoluir esse projeto.

## **ATENÇÃO: Não passe esse código para pessoas que não fizeram o curso ainda!!!**

Passos para preparação do ambiente de desenvolvimento no Serpro
- Baixar o Android Studio e descompactar
- Baixar o Java JDK
- Configurar o JDK (.bashrc/.profile)
    ```
    export APPS=/home/<CPF>/<PASTA COM SDKs>

    export JAVA_HOME=$APPS/jdk1.8.0_92
    export IDEA_JDK=$APPS/jdk1.8.0_92
    export JDK_HOME=$APPS/jdk1.8.0_92
    export ANDROID_HOME=$APPS/android-sdk-linux

    export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"

    export PATH=$JAVA_HOME/bin:$APPS/android-sdk-linux/platform-tools:$PATH
    ```
- Na pasta /bin do Android Studio extraído (descompactado), executar o arquivo studio.sh
- Instalar o git (`sudo apt-get install git`)
- Se houver problemas com o git do Serpro (problemas com o certificado), utilizar o comando `git config --global http.sslverify false`


| **Git - Comandos úteis** |  |
|---|---|
| Criar novo repositório | `git init` |
| Obter um repositório (local) | `git clone /caminho/para/o/repositório` |
| Obter um repositório (remoto) | `git clone usuário@servidor:/caminho/para/o/repositório` |
| Adicionar um arquivo | `git add <arquivo>` |
| Confirmar mudança (para HEAD) | `git commit -m "comentários das alterações"` |
| Enviar alterações | `git push origin master` |
| Atualizar repositório | `git pull` |
| Criar um branch e mudar para o mesmo | `git checkout -b funcionalidade_x` |
| Retorna para o master | `git checkout master` |
| Remover o branch | `git branch -d funcionalidade_x` |
| Enviar o branch para o server remoto | `git push origin funcionalidade_x` |
| Merge de outro branch ao seu branch ativo | `git merge outro_branch` |
| Alterar URL do repositório remoto | `git remote set-url origin https://nova.url.aqui` |

| **ADB** |  |
|---|---|
| Listar dispositivos por serial | `adb devices` |
| Listar dispositivos por produto/modelo | `adb devices -l` |
| Instalar o apk | `adb install <path>` |
| Matar o servidor (se rodando) | `adb kill-server` |
| Inicia o servidor | `adb start-server` |

| **Links úteis** |  |
|---|---|
| Android & Tech (novidades) | https://medium.com/android-news |
| Android Asset Studio | https://romannurik.github.io/AndroidAssetStudio/ |
| Android Awesome (libs) | https://github.com/snowdream/awesome-android |
| Android Experiments | https://www.androidexperiments.com/ |
| Android Weekly (novidades) | http://androidweekly.net/ |
| Bitbucket (repositório GIT privado) | https://bitbucket.org |
| Blog de AI (UX) | http://arquiteturadeinformacao.com/ |
| Codelab | https://search-codelabs.appspot.com/ |
| Developer Android (guias e documentação) | https://developer.android.com |
| Fabric | https://fabric.io |
| Git do Serpro (Intranet) | https://git.serpro |
| Git Guia Rápido | http://rogerdudler.github.io/git-guide/index.pt_BR.html |
| Google Dashboards | https://developer.android.com/about/dashboards/index.html |
| Google Play Developers Console | https://play.google.com/apps/publish/ |
| Lista de APIs públicas | https://github.com/toddmotto/public-apis |
| Map Icons (ícones para mapas) | https://mapicons.mapsmarker.com/ |
| Material Design | http://www.materialup.com |
| Material Icons | https://design.google.com/icons/ |
| Material Palette (paleta de cores) | http://www.materialpalette.com |
| Noun Project (fonte de ícones) | https://thenounproject.com |
| PAD (Intranet) | http://pad.serpro |
| Paleta de cores do Material Design | https://www.materialui.co/colors |
| POP - Prototyping On Paper | https://popapp.in/ |
| The Android Arsenal (libs, tools) | http://android-arsenal.com/ |
| Udacity (cursos) | https://www.udacity.com |
| UX Planet | https://uxplanet.org/ |
| Vogella (tutoriais) | http://www.vogella.com/tutorials/android.html |